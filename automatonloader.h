#ifndef AUTOMATONLOADER_H
#define AUTOMATONLOADER_H

#include <QColor>
#include <QImage>
#include <QWidget>
#include <QDialog>
#include <vector>

class AutomatonLoader
{
public:
     void AutomatonLoder();
     void createAutomaton(int identifier, QWidget *parent);

protected:
};

#endif // AUTOMATONLOADER_H
