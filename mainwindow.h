#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:


private slots:
    void on_actionLoad_triggered();
    void automatonChanged(int index);


private:
    Ui::MainWindow *ui;
    void initializeAutomataCombobox();

    int selectedAutomaton;

};

#endif // MAINWINDOW_H
