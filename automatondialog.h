#ifndef AUTOMATONDIALOG_H
#define AUTOMATONDIALOG_H

#include <QDialog>

namespace Ui {
class AutomatonDialog;
}

class AutomatonDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AutomatonDialog(QWidget *parent = 0);
    ~AutomatonDialog();
    int getRule();
    int getSteps();

private:
    Ui::AutomatonDialog *ui;
};

#endif // AUTOMATONDIALOG_H
