#include "mainwindow.h"
#include <QComboBox>
#include "ui_mainwindow.h"
#include "automatonnetwork.h"
#include "automatonloader.h"
#include "Input/elementary1dloader.h"
#include "Input/elementary2dloader.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    initializeAutomataCombobox();

}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::initializeAutomataCombobox()
{
    QComboBox* automatonSelection = new QComboBox();
    automatonSelection->insertItem(0, QIcon(), "1D ElementaryAutomaton");
    automatonSelection->insertItem(1, QIcon(), "2D ElementaryAutomaton");
    ui->mainToolBar->insertSeparator(ui->actionLoad);
    ui->mainToolBar->insertWidget(ui->actionLoad, automatonSelection);
    ui->mainToolBar->insertSeparator(ui->actionLoad);
    this->connect(automatonSelection, SIGNAL(currentIndexChanged(int)), this, SLOT(automatonChanged(int)));

}

void MainWindow::automatonChanged(int index)
{
    selectedAutomaton = index;
}

void MainWindow::on_actionLoad_triggered()
{
    AutomatonLoader automaton;
    automaton.createAutomaton(selectedAutomaton, this);

    AutomatonNetwork client;
    client.newConnection();
    client.sendData();
//    client.closeConnection();
}
