#include "mainwindow.h"
#include <QApplication>
#include "automatonnetwork.h"
#include "automatonloader.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
