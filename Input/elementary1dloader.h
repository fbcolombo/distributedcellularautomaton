#ifndef ELEMENTARY1DLOADER_H
#define ELEMENTARY1DLOADER_H

#include <QColor>
#include <QImage>
#include <QWidget>
#include <QDialog>
#include <vector>

class Elementary1DLoader
{
public:
     Elementary1DLoader(int identifier, QWidget* parent);
     void createAutomaton();

protected:
    void fillData(QImage& image);

    std::vector<int> cellsData;
    QColor particleColor;
    int rule;
    int id;
    int steps;
};

#endif // ELEMENTARY1DLOADER_H
