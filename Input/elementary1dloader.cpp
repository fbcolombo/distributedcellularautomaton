#include "elementary1dloader.h"
#include "../automatondialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <vector>
#include <QDomDocument>
#include <QFile>
#include <QDebug>

Elementary1DLoader::Elementary1DLoader(int identifier, QWidget* parent) : id(identifier)
{
    particleColor = Qt::black;

    AutomatonDialog form(parent);

    form.exec();
    rule = form.getRule();
    steps = form.getSteps();


    QString fileName = QFileDialog::getOpenFileName(parent, "Open Image", "/users/Daniel/Documents/CellularAutomata3D/images/elementaryautomaton.png", "Image Files (*.png *.jpg *.bmp)");

    QImage image;
    if (image.load(fileName, 0))
    {
        cellsData.resize(image.width());
        fillData(image);
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setModal(true);
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("Could not open input file!");
        msgBox.exec();
    }
}

void Elementary1DLoader::createAutomaton()
{
    QString idString = QString::number(id);
    QString ruleString = QString::number(rule);
    QString stepsString = QString::number(steps);
    QString cellsDataString;
    for (int i = 0; i < cellsData.size(); ++i)
        cellsDataString.append(QString::number(cellsData[i]));

    qDebug() << cellsDataString;



    QDomDocument doc;

    QDomProcessingInstruction header = doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
    doc.appendChild( header );

    QDomElement root = doc.createElement("automaton");
    doc.appendChild(root);

    QDomElement id = doc.createElement("id");
    root.appendChild(id);

    QDomText ta = doc.createTextNode(idString);
    id.appendChild(ta);

    QDomElement rule = doc.createElement("rule");
    root.appendChild(rule);

    QDomText tb = doc.createTextNode(ruleString);
    rule.appendChild(tb);

    QDomElement steps = doc.createElement("steps");
    root.appendChild(steps);

    QDomText tc = doc.createTextNode(stepsString);
    steps.appendChild(tc);

    QDomElement state = doc.createElement("state");
    root.appendChild(state);

    QDomText te = doc.createTextNode(cellsDataString);
    state.appendChild(te);

    QString filename = "/users/Daniel/Documents/client.xml";

    QFile file(filename);

    if( !file.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
    qDebug( "Failed to open file for writing." );
    }

    QTextStream stream(&file);
    stream << doc.toString();
    file.close();


}

void Elementary1DLoader::fillData(QImage &image)
{
    for (size_t ii = 0; ii < cellsData.size(); ii++)
    {
        QColor pixelColor(image.pixel(ii, 0));

        int state = 0;
        if (pixelColor == particleColor)
        {
            state = 1;
        }

        cellsData[ii] = state;

    }
}

