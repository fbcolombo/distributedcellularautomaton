#include "elementary2dloader.h"
#include "../automatondialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <vector>
#include <QXmlStreamWriter>
#include <QFile>
#include <QDebug>

Elementary2DLoader::Elementary2DLoader(int identifier, QWidget* parent) : id(identifier)
{
    particleColor = Qt::black;

    AutomatonDialog form(parent);

    form.exec();
    rule = form.getRule();
    maxSteps = form.getSteps();


    QString fileName = QFileDialog::getOpenFileName(parent, "Open Image", "/users/Daniel/Documents/CellularAutomata3D/images/elementaryautomaton.png", "Image Files (*.png *.jpg *.bmp)");

    QImage image;
    if (image.load(fileName, 0))
    {
        cellsData.resize(image.width());
        fillData(image);
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setModal(true);
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("Could not open input file!");
        msgBox.exec();
    }
}

void Elementary2DLoader::createAutomaton()
{
    QString idString = QString::number(id);
    QString maxStepsString = QString::number(maxSteps);
    QString ruleString = QString::number(rule);
    QString cellsDataString;
    for (int i = 0; i < cellsData.size(); ++i)
        cellsDataString.append(QString::number(cellsData[i]));

    qDebug() << cellsDataString;


    QString filename = "/home/daniel/Daniel/teste.xml";

    QFile file(filename);
    file.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("Automaton");
    xmlWriter.writeAttribute("id", idString);

    xmlWriter.writeTextElement("maxSteps", maxStepsString);
    xmlWriter.writeTextElement("rule", ruleString);
    xmlWriter.writeTextElement("State", cellsDataString);

    xmlWriter.writeEndElement();

    file.close();

}

void Elementary2DLoader::fillData(QImage &image)
{
    for (size_t ii = 0; ii < cellsData.size(); ii++)
    {
        QColor pixelColor(image.pixel(ii, 0));

        int state = 0;
        if (pixelColor == particleColor)
        {
            state = 1;
        }

        cellsData[ii] = state;

    }
}
