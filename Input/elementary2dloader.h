#ifndef ELEMENTARY2DLOADER_H
#define ELEMENTARY2DLOADER_H

#include <QColor>
#include <QImage>
#include <QWidget>
#include <QDialog>
#include <vector>

class Elementary2DLoader
{
public:
     Elementary2DLoader(int identifier, QWidget* parent);
     void createAutomaton();

protected:
    void fillData(QImage& image);

    std::vector<int> cellsData;
    QColor particleColor;
    int rule;
    int maxSteps;
    int squareSize;
    int id;
};

#endif // ELEMENTARY2DLOADER_H
