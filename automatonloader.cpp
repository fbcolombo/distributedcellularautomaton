#include "automatonloader.h"
#include "Input/elementary1dloader.h"
#include "Input/elementary2dloader.h"
#include "automatondialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <vector>
#include <QXmlStreamWriter>
#include <QFile>
#include <QDebug>


void AutomatonLoader::AutomatonLoder()
{
}

void AutomatonLoader::createAutomaton(int identifier, QWidget *parent)
{
    switch (identifier)
    {
        case 0:
        {
            Elementary1DLoader loader(identifier, parent);
            loader.createAutomaton();
            break;
        }
        case 1:
        {
            Elementary2DLoader loader(identifier, parent);
            loader.createAutomaton();
            break;
        }
    }
}
