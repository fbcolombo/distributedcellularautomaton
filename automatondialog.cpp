#include "automatondialog.h"
#include "ui_automatondialog.h"

AutomatonDialog::AutomatonDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AutomatonDialog)
{
    ui->setupUi(this);
}

AutomatonDialog::~AutomatonDialog()
{
    delete ui;
}

int AutomatonDialog::getRule()
{
    int rule = ui->ruleEdit->text().toInt();
    return rule;
}

int AutomatonDialog::getSteps()
{
    int steps = ui->stepsEdit->text().toInt();
    return steps;
}
