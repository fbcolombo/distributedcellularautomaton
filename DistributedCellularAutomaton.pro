#-------------------------------------------------
#
# Project created by QtCreator 2017-09-05T16:43:24
#
#-------------------------------------------------

QT       += core gui
QT       += network
QT       += xml


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DistributedCellularAutomaton
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    automatonnetwork.cpp \
    automatonloader.cpp \
    automatondialog.cpp \
    Input/elementary1dloader.cpp \
    Input/elementary2dloader.cpp

HEADERS  += mainwindow.h \
    automatonnetwork.h \
    automatonloader.h \
    automatondialog.h \
    Input/elementary1dloader.h \
    Input/elementary2dloader.h

FORMS    += mainwindow.ui \
    automatondialog.ui
